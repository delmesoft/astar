package org.delmesoft.astar;

import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;
public abstract class AStar {
	
	public static final float LATERAL_WEIGHT = 1f;
	public static final float DIAGONAL_WEIGHT = (float) (Math.sqrt(3) * LATERAL_WEIGHT);
		
	private Set<Node> closedSet;
	private PriorityQueue<Node> openSet;

	private Heuristic heuristic;
	
	public AStar(Heuristic heuristic) {
		
		this.heuristic = heuristic;
		
		closedSet = new HashSet<Node>();
		openSet = new PriorityQueue<Node>();
				
	}

	public boolean route(int srcX, int srcY, int srcZ, int tgtX, int tgtY, int tgtZ, int depth, List<Node> route) {
		
		return route(getNode(tgtX, tgtY, tgtZ), getNode(srcX, srcY, srcZ), depth, route);
	}

	private boolean route(Node src, Node tgt, int depth, List<Node> route) {
						
		final Set<Node> closedSet = this.closedSet;
		closedSet.clear();
		
		final PriorityQueue<Node> openSet = this.openSet;
		openSet.clear();

		src.g = 0;
		src.h = heuristic.compute(src, tgt);
		src.f = src.g + src.h;

		openSet.add(src);
		
		do {
			
			final Node node = openSet.poll();
			
			if(node == tgt) {
				backTrace(src, tgt, route);
				return true;
			}
			
			// Change this if you want to limit the world (e.g.: minY = 0 and maxY = 256) 
			int minX = Math.max(src.x - depth, node.x - 1);
			int minY = Math.max(src.y - depth, node.y - 1);
			int minZ = Math.max(src.z - depth, node.z - 1);
			
			int maxX = Math.min(src.x + depth, node.x + 2);
			int maxY = Math.min(src.y + depth, node.y + 2);
			int maxZ = Math.min(src.z + depth, node.z + 2);
			
			for(int x = minX; x < maxX; x++) {
				for(int y = minY; y < maxY; y++) {
					for(int z = minZ; z < maxZ; z++) {
												
						if(x != node.x || y != node.y || z != node.z) {
							
							final Node neighbor = getNode(x, y, z);

							if (neighbor.obstacle || closedSet.contains(neighbor) || !isNodeAccessible(node, neighbor)) continue;
														
							float weight = (node.x == x || node.z == z) && (node.y == y) ? LATERAL_WEIGHT : DIAGONAL_WEIGHT;

							float g = node.g + weight;
							float h = heuristic.compute(neighbor, tgt);
							float f = g + h;

							if(neighbor.f > f) {

								neighbor.g = g;
								neighbor.h = h;
								neighbor.f = f;
								neighbor.parent = node;

								openSet.remove(neighbor); // FORCE UPDATE
								openSet.add(neighbor);

							} else if(!openSet.contains(neighbor)) {

								neighbor.g = g;
								neighbor.h = h;
								neighbor.f = f;
								neighbor.parent = node;

								openSet.add(neighbor);

							}

						}
					}					
				}
			}			
			
			closedSet.add(node);
			
		} while (openSet.size() > 0);
		
		return false;
	}
	
	private void backTrace(Node n1, Node n2, List<Node> route) {

		// normal
		int nX = 0, nY = 0, nZ = 0;

		Node lastNode;

		while (n2 != n1) {

			lastNode = n2; // lastNode
			n2 = n2.parent; // currentNode

			int x = n2.x - lastNode.x, 
				y = n2.y - lastNode.y, 
				z = n2.z - lastNode.z;

			if (x != nX || y != nY || z != nZ) {

				nX = x;
				nY = y;
				nZ = z;

				route.add(lastNode);

			}

		}

		route.add(n1);

	}
	
	/**
	 * Test neighbor nodes. e.g. corners, floor ...
	 * @param src
	 * @param tgt
	 * @return
	 */
	abstract boolean isNodeAccessible(Node src, Node tgt);

	abstract Node getNode(int x, int y, int z);
	
	public interface Heuristic {

		float compute(Node node, Node tgt);

	}

}
