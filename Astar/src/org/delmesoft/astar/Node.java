package org.delmesoft.astar;

public class Node implements Comparable<Node> {

	public final int x, y, z;

	float f; // Coste estimado de la solución a través de n.
	float g; // Coste real hasta el nodo n.
	float h; // Coste estimado hasta el objetivo

	public boolean obstacle;

	Node parent;

	public Node(int x, int y, int z, boolean obstacle) {

		this.x = x;
		this.y = y;
		this.z = z;
		
		this.obstacle = obstacle;
		
	}

	public String toString() {
		return x + ", " + y + ", " + z;
	}
	
	@Override
	public int compareTo(Node o) {
		
		if (f < o.f)
			return -1;
		if (f > o.f)
			return 1;

		return (h < o.h) ? -1 : ((h == o.h) ? 0 : 1);
	}
	
}
