package org.delmesoft.astar;

import org.delmesoft.astar.AStar.Heuristic;
import static org.delmesoft.astar.AStar.LATERAL_WEIGHT;

public class ManhattanDistance implements Heuristic {

	@Override
	public float compute(Node src, Node tgt) {
		return (Math.abs(src.x - tgt.x) + Math.abs(src.y - tgt.y) + Math.abs(src.z - tgt.z)) * LATERAL_WEIGHT;
	}
	
}
